#!/bin/bash
# capture arguments
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
	-f|--fastqdir)
	fastqdir="$2"
	shift
	shift
	;;
	-o|--outdir)
	outdir="$2"
	shift
	shift
	;;
    	*)    # unknown option
    	POSITIONAL+=("$1") # save it in an array for later
   	shift # past argument
    	;;
esac
done
set -- "${POSITIONAL[@]}"
# create directory for trimmed reads
mkdir -p $outdir/trimmomatic
# looping through fastq files
for i in $fastqdir/*.fastq.gz
do
# filebasename
samp=$(basename $i)
echo "Starting trimming for $samp..."
# trimmomatic
java -jar /home/martin/software/Trimmomatic-0.36/trimmomatic-0.36.jar SE -threads 7 -phred33 $i $outdir/trimmomatic/$samp ILLUMINACLIP:/home/martin/software/Trimmomatic-0.36/adapters/TruSeq3-SE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36 2>>$outdir/trimmomatic_log.txt
echo "Trimming for $samp completed"
done
echo "Trimmomatic completed"



