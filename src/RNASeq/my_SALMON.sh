#!/bin/bash
# capture arguments
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
	-f|--fastqdir)
	fastqdir="$2"
	shift
	shift
	;;
	-o|--outdir)
	outdir="$2"
	shift
	shift
	;;
	-i|--index)
	index="$2"
	shift
	shift
	;;
    	*)    # unknown option
    	POSITIONAL+=("$1") # save it in an array for later
   	shift # past argument
    	;;
esac
done
set -- "${POSITIONAL[@]}"
# looping through fastq files
for i in $fastqdir/*.fastq.gz
do
# filebasename
samp=$(basename $i)
# seqtk
echo "Quantifying $samp with SALMON..."
salmon quant -p 6 -i $index -l A -r $i -o $outdir/${samp/.fastq.gz/}
done
echo "Quantification with SALMON completed!"
