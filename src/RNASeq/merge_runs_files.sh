#!/bin/bash
# capture arguments
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
	-d1|--dir1)
	dir1="$2"
	shift
	shift
	;;
	-d2|--dir2)
	dir2="$2"
	shift
	shift
	;;
	-o|--outdir)
	outdir="$2"
	shift
	shift
	;;
    	*)    # unknown option
    	POSITIONAL+=("$1") # save it in an array for later
   	shift # past argument
    	;;
esac
done
set -- "${POSITIONAL[@]}"
# looping through fastq files
for i in $dir1/*.fastq.gz
do
# filebasename
samp=$(basename $i)
# seqtk
echo "Merging $samp..."
# cat both .fastq gz files
cat $dir1/$samp $dir2/$samp > $outdir/$samp
done
echo "Merging completed!"
 
