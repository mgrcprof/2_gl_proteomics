#!/bin/bash
# capture arguments
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
	-f|--fastqdir)
	fastqdir="$2"
	shift
	shift
	;;
	-o|--outdir)
	outdir="$2"
	shift
	shift
	;;
	-i|--index)
	index="$2"
	shift
	shift
	;;
    	*)    # unknown option
    	POSITIONAL+=("$1") # save it in an array for later
   	shift # past argument
    	;;
esac
done
set -- "${POSITIONAL[@]}"
# create directory for alignments and summary
mkdir -p $outdir/sam
mkdir -p $outdir/summary
# looping through fastq files
for i in $fastqdir/*.fastq.gz
do
# filebasename
samp=$(basename $i)
# seqtk
echo "Aligning... $samp"
hisat2 -p 6 -x $index --new-summary --summary-file $outdir/summary/${samp/.fastq.gz/_summary.txt} -U $i -S $outdir/sam/${samp/.fastq.gz/.sam}
done
echo "Alignment with HISAT2 completed!"
